using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/* Programm zur Berechnung des Teppichbodenverlegens 
 * Eingaben: Länge und Breite des Raums
 * Ausgaben: Fläche, Nettopreise, Gesamtpreis brutto
 * Autor: Frank Herzog
 * Datum: 24. September 2015
 */

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form   // Klassendefinition Klasse Teppichboden
    {
        public Form1()  // Konstruktor
        {
            InitializeComponent();
            // Hilfeanzeige für die Buttons
            ToolTip tp = new ToolTip(); // Objekt tp instanziert
            // Hilfeanzeige für jeden einzelnen Button codieren
            tp.SetToolTip(cmd_berechnen, "Berechnen des Gesamtpreises");
            tp.SetToolTip(cmd_clear, "Eingaben löschen");
            tp.SetToolTip(cmd_end, "Program beenden");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            // Button zum Programm beenden
            Application.Exit();
        }

        private void cmd_berechnen_Click(object sender, EventArgs e)
        {
            // Try-Block (Versuche)
            try
            {
                // Variablendeklanation
                double laenge, breite, flaeche, nettopreisteppich, nettopreisverlegen, gesamtpreis;
                // Konvertieren der Texteingabe in den jeweiligen Datentyp: double
                laenge = Convert.ToDouble(txt_laenge.Text);
                breite = Convert.ToDouble(txt_breite.Text);
                // Berechnungen
                flaeche = laenge * breite;
                nettopreisteppich = flaeche * 35;
                nettopreisverlegen = flaeche * 22;
                gesamtpreis = (nettopreisteppich + nettopreisverlegen) * 1.19;
                /* Ausgaban
                 * formatieren der Double-Variablen in Strings mit zwei Nachkommastellen
                 * Zeilenschaltung mit envoirement.NewLine
                 * Stringverknüpfung mit Operator +
                 * (Für jede neue Zeile muss ein * zuvor gesetzt werden, sonst wird die vorherige gelöscht)
                 */
                // Geamtpreis ab 2500,- € 5% Rabatt
                if (gesamtpreis >= 2500)
                    gesamtpreis *= 0.95;    // gesamtpreis = gesamtpreis * 0.95
                txt_output.Text = "Fläche des Raums: " + flaeche.ToString("F2") + "m²" + Environment.NewLine;
                txt_output.Text += "Nettopreis Teppichboden: " + nettopreisteppich.ToString("F2") + " €" + Environment.NewLine;
                txt_output.Text += "Nettopreis Verlegen: " + nettopreisverlegen.ToString("F2") + " €" + Environment.NewLine;
                txt_output.Text += "Gesamtpreis: " + gesamtpreis.ToString("F2") + " €" + Environment.NewLine;
            }
            // Catch-Block (Fangen)
            catch (Exception ex)
            {
                // Fehlermeldung des Systems ausgeben
                MessageBox.Show(ex.Message);
                // Anzeige löschen
                txt_breite.Text = txt_breite.Text = "";
                // Focus für nächsten Programmdurchlauf
                txt_laenge.Focus();
            }
        }

        // Anzeigen löschen
        private void cmd_clear_Click(object sender, EventArgs e)
        {
            // Textboxen löschen
            txt_output.Text = txt_breite.Text = txt_laenge.Text = "";
            // Focus (CUrsor setzen
            txt_laenge.Focus();
        }
    }
}