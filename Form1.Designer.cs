namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_laenge = new System.Windows.Forms.TextBox();
            this.txt_breite = new System.Windows.Forms.TextBox();
            this.cmd_berechnen = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Länge";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Breite";
            // 
            // txt_laenge
            // 
            this.txt_laenge.Location = new System.Drawing.Point(157, 40);
            this.txt_laenge.Name = "txt_laenge";
            this.txt_laenge.Size = new System.Drawing.Size(100, 22);
            this.txt_laenge.TabIndex = 1;
            // 
            // txt_breite
            // 
            this.txt_breite.Location = new System.Drawing.Point(157, 87);
            this.txt_breite.Name = "txt_breite";
            this.txt_breite.Size = new System.Drawing.Size(100, 22);
            this.txt_breite.TabIndex = 2;
            // 
            // cmd_berechnen
            // 
            this.cmd_berechnen.Location = new System.Drawing.Point(319, 40);
            this.cmd_berechnen.Name = "cmd_berechnen";
            this.cmd_berechnen.Size = new System.Drawing.Size(85, 23);
            this.cmd_berechnen.TabIndex = 3;
            this.cmd_berechnen.Text = "&Berechnen";
            this.cmd_berechnen.UseVisualStyleBackColor = true;
            this.cmd_berechnen.Click += new System.EventHandler(this.cmd_berechnen_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(319, 85);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(85, 23);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(319, 203);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(85, 23);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(54, 203);
            this.txt_output.Multiline = true;
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_output.Size = new System.Drawing.Size(243, 139);
            this.txt_output.TabIndex = 6;
            this.txt_output.TabStop = false;
            //this.txt_output.TextChanged += new System.EventHandler(this.txt_output_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 376);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_berechnen);
            this.Controls.Add(this.txt_breite);
            this.Controls.Add(this.txt_laenge);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Teppich verlegen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_laenge;
        private System.Windows.Forms.TextBox txt_breite;
        private System.Windows.Forms.Button cmd_berechnen;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.TextBox txt_output;
    }
}

